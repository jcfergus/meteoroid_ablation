//
// Created by jcfergus on 10/26/22.
//

#include "Meteoroid.h"

Meteoroid::Meteoroid(double mass_init, double temp_init, double v_init, double h_init, double angle_init){
    this->mass_init = mass_init;
    this->temp_init = temp_init;
    this->v_init = v_init;
    this->h_init = h_init;
    this->angle_init = angle_init;

    this->mass = mass_init;
    this->height_m = h_init;
    this->angle = angle_init;
    this->vel = v_init;
}
