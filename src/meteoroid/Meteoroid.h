//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_METEOROID_H
#define METEOROID_ABLATION_METEOROID_H

#include "../heat_flux/HeatFlux.h"
#include "../line_of_sight/LineOfSight.h"
#include "../atmosphere/Atmosphere.h"
#include "../drag/Drag.h"
#include "../radiation/Radiation.h"
#include "../vaporization/Vaporization.h"
#include "../rotation/Rotation.h"

class Atmosphere;
class Drag;
class HeatFlux;
class LineOfSight;
class Rotation;
class Radiation;
class Vaporization;

class Meteoroid {

public:
    Meteoroid(double mass_init, double temp_init, double v_init, double h_init, double angle_init);

    virtual bool Calculate_Line_Of_Sight() = 0;
    virtual bool Calculate_Heat_Flux() = 0;
    virtual bool Calculate_Rotation() = 0;
    virtual bool Calculate_Drag() = 0;
    virtual bool Calculate_Vaporization() = 0;
    virtual bool Apply_Heat_Transfer(double dt) = 0;
    virtual bool Apply_Shape_Change(double dt) = 0;
    virtual bool Apply_Rotation(double dt) = 0;
    virtual bool Apply_Drag(double dt) = 0;

    // Initial state variables
    double mass_init, temp_init, v_init, h_init, angle_init;

    // Dynamic state variables
    double mass, vel, height_m, angle;

    Atmosphere* atmos_model;
    Drag* drag_model;
    HeatFlux* heatflux_model;
    LineOfSight* lineofsight_model;
    Rotation* rot_model;
    Radiation* rad_model;
    Vaporization* vap_model;


};


#endif //METEOROID_ABLATION_METEOROID_H
