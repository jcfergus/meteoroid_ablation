//
// Created by jcfergus on 10/26/22.
//

#include <math.h>

#include <iostream>

#include "Meteoroid_0D.h"
#include "../heat_flux/HeatFlux_0D.h"
#include "../atmosphere/AtmosphereNrlmsise00.h"
#include "../drag/Drag_0D.h"
#include "../vaporization/Vaporization_0D.h"
#include "../radiation/Radiation_0D.h"

Meteoroid_0D::Meteoroid_0D(double mass_init, double temp_init, double v_init, double h_init, double angle_init,
                           double cp, double rho, double molecular_mass, double shape_factor,
                           double heat_transfer_coeff)
        : Meteoroid(mass_init, temp_init, v_init, h_init, angle_init){

    this->cp = cp;
    this->rho = rho;
    this->molecular_mass = molecular_mass;
    this->temp = temp_init;
    this->shape_factor = shape_factor;
    this->heat_transfer_coeff = heat_transfer_coeff;


    atmos_model = new AtmosphereMSISE();
    drag_model = new Drag_0D();
    heatflux_model = new HeatFlux_0D();
    vap_model = new Vaporization_0D(10.6, 13500, 6e6);
    rad_model = new Radiation_0D(0.9);

}

Meteoroid_0D::~Meteoroid_0D() {
    delete atmos_model;
    delete drag_model;
    delete heatflux_model;
    delete vap_model;
    delete rad_model;
}


bool Meteoroid_0D::Calculate_Line_Of_Sight() {
    return true;
}

bool Meteoroid_0D::Calculate_Heat_Flux(){
    bool atmos_flux = heatflux_model->CalculateHeatFlux(this, atmos_model);
    if(!atmos_flux) return false;

    return rad_model->CalculateHeatFlux(this, atmos_model);
}

bool Meteoroid_0D::Calculate_Rotation(){
    return true;
}

bool Meteoroid_0D::Calculate_Drag(){
    return drag_model->CalculateDrag(this, atmos_model);
}

bool Meteoroid_0D::Calculate_Vaporization(){
    return vap_model->CalculateVaporization(this);
}

bool Meteoroid_0D::Apply_Heat_Transfer(double dt){
//    std::cout << "dTdt: " << (heatflux_atmos + heatflux_vaporization + heatflux_radiation) << std::endl;
    temp = temp + dt * (heatflux_atmos + heatflux_vaporization + heatflux_radiation);
    return true;
}

bool Meteoroid_0D::Apply_Shape_Change(double dt){
    mass = mass + dt * dmdt;
    return true;
}

bool Meteoroid_0D::Apply_Rotation(double dt){
    return true;
}

bool Meteoroid_0D::Apply_Drag(double dt){
//    std::cout << "dvdt: " << dvdt << " " << dt << std::endl;
    vel = vel + dt * dvdt;
    height_m = height_m - dt * vel * cos(angle);
    return true;
}