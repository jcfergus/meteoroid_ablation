//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_METEOROID_0D_H
#define METEOROID_ABLATION_METEOROID_0D_H

#include "Meteoroid.h"

class Meteoroid_0D : public Meteoroid{
public:
    Meteoroid_0D(double mass_init, double temp_init, double v_init, double h_init, double angle_init,
                 double cp, double rho, double molecular_mass, double shape_factor,
                 double heat_transfer_coeff);
    ~Meteoroid_0D();

    bool Calculate_Line_Of_Sight();
    bool Calculate_Heat_Flux();
    bool Calculate_Rotation();
    bool Calculate_Drag();
    bool Calculate_Vaporization();
    bool Apply_Heat_Transfer(double dt);
    bool Apply_Shape_Change(double dt);
    bool Apply_Rotation(double dt);
    bool Apply_Drag(double dt);

    // Material properties
    double cp, rho, molecular_mass, temp, shape_factor, heat_transfer_coeff;

    // Heat Flux Terms
    double heatflux_atmos, heatflux_radiation, heatflux_vaporization;

    // Drag Terms
    double dvdt;

    // Double Vaporization Terms
    double dmdt;


};


#endif //METEOROID_ABLATION_METEOROID_0D_H
