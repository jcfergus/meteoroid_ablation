//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_VAPORIZATION_0D_H
#define METEOROID_ABLATION_VAPORIZATION_0D_H

#include "Vaporization.h"

class Vaporization_0D : public Vaporization{
public:
    Vaporization_0D(double C_A, double C_B, double LHofV);
    bool CalculateVaporization(Meteoroid *meteoroid);

private:
    double C_A, C_B; // Clausius Clapeyron coefficients
    double LHofV; // Heat of vaporization
};


#endif //METEOROID_ABLATION_HEATFLUX_0D_H
