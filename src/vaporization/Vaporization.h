//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_VAPORIZTION_H
#define METEOROID_ABLATION_VAPORIZTION_H

#include "../meteoroid/Meteoroid.h"

class Meteoroid;

class Vaporization {
public:
    virtual bool CalculateVaporization(Meteoroid *meteoroid) = 0;
};


#endif //METEOROID_ABLATION_VAPORIZTION_H
