//
// Created by jcfergus on 10/26/22.
//

#include "Vaporization_0D.h"
#include "../meteoroid/Meteoroid_0D.h"
#include <math.h>
#include <iostream>

#define KB 1.381e-23

Vaporization_0D::Vaporization_0D(double C_A, double C_B, double LHofV){
    this->C_A = C_A;
    this->C_B = C_B;
    this->LHofV = LHofV;
}

bool Vaporization_0D::CalculateVaporization(Meteoroid *meteoroid){
    Meteoroid_0D* met0D = ((Meteoroid_0D*)meteoroid);

    double Pv = pow(10, C_A - C_B / met0D->temp);

    met0D->dmdt = -4 * met0D->shape_factor * pow(met0D->mass/met0D->rho, 2./3.) *
            Pv * sqrt(met0D->molecular_mass / (2 * M_PI * KB * met0D->temp));

    double mult = 1. / ( met0D->cp * pow(met0D->mass, 1./3.) * pow(met0D->rho, 2./3.) / met0D->shape_factor);
    met0D->heatflux_vaporization = LHofV / met0D->shape_factor * pow(met0D->rho/met0D->mass, 2./3.) * mult * met0D->dmdt;

    return true;
}