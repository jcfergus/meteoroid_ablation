//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_Rotation_H
#define METEOROID_ABLATION_Rotation_H

#include "../meteoroid/Meteoroid.h"

class Meteoroid;

class Rotation {
public:
    virtual bool CalculateRotation(Meteoroid *meteoroid) = 0;
    virtual bool ApplyRotation(Meteoroid *meteoroid) = 0;
};


#endif //METEOROID_ABLATION_Rotation_H
