//
// Created by jcfergus on 11/4/22.
//

#ifndef METEOROID_ABLATION_PLOTTER_H
#define METEOROID_ABLATION_PLOTTER_H

#include <vector>

class Plotter {
public:
    static void dump_data(std::vector<double> *x, std::vector<double> *y, std::string name);

};


#endif //METEOROID_ABLATION_PLOTTER_H
