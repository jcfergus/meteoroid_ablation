//
// Created by jcfergus on 11/4/22.
//

#include "Plotter.h"

#include <fstream>
#include <iterator>
#include <string>
#include <vector>

void Plotter::dump_data(std::vector<double> *x, std::vector<double> *y, std::string name) {

    std::ofstream outFile_x("../python_scripts/plot/"+name+"x.txt");
    for (const auto &e : *x) outFile_x << e << ",";

    std::ofstream outFile_y("../python_scripts/plot/"+name+"y.txt");
    for (const auto &e : *y) outFile_y << e << ",";

}