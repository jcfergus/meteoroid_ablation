#include "AblationSolver.h"
#include "meteoroid/Meteoroid_0D.h"

#include <iostream>

using namespace met;

AblationSolver::AblationSolver(Meteoroid *meteoroid) {
    this->meteoroid = meteoroid;
}

bool AblationSolver::RunSolver_nSteps(double dt, int num_steps) {
    for(int i=0;i<num_steps;i++){
        if(!RunTimeStep(dt)) return false;
    }
    return true;
}

bool AblationSolver::RunSolver_MassCutoff(double dt, double mass_minimum) {
    while(meteoroid->mass > mass_minimum){
        if(!RunTimeStep(dt)) return false;
    }
    return true;
}

bool AblationSolver::RunTimeStep(double dt) {

    std::cout << "step: " << mass.size() << "  h:" << meteoroid->height_m << " " << "  m:" << meteoroid->mass << " " << "  v:" << meteoroid->vel << " " << "  T:" << ((Meteoroid_0D*)meteoroid)->temp << std::endl;

    if(!meteoroid->Calculate_Line_Of_Sight()) return false;

    if(!meteoroid->Calculate_Heat_Flux()) return false;

    if(!meteoroid->Calculate_Drag()) return false;

    if(!meteoroid->Calculate_Vaporization()) return false;

    if(!meteoroid->Apply_Heat_Transfer(dt)) return false;

    if(!meteoroid->Apply_Shape_Change(dt)) return false;

    if(!meteoroid->Apply_Rotation(dt)) return false;

    if(!meteoroid->Apply_Drag(dt)) return false;

    mass.push_back(meteoroid->mass);
    height.push_back(meteoroid->height_m);
    velocity.push_back(meteoroid->vel);

    //TEMPORARY
    temperature.push_back(((Meteoroid_0D*)meteoroid)->temp);

    return true;
}