//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_RADIATION_0D_H
#define METEOROID_ABLATION_RADIATION_0D_H

#include "Radiation.h"

class Radiation_0D : public Radiation{
public:
public:
    Radiation_0D(double emissivity);
    bool CalculateHeatFlux(Meteoroid *meteoroid, Atmosphere *atmos);


private:
    double emissivity_;

};


#endif //METEOROID_ABLATION_RADIATION_0D_H
