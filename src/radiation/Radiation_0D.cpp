//
// Created by jcfergus on 10/26/22.
//

#include "Radiation_0D.h"
#include "../meteoroid/Meteoroid_0D.h"
#include <math.h>

// Stefan-Boltzmann constan
#define SIGMA 5.670374419e-8

Radiation_0D::Radiation_0D(double emissivity) {
    this->emissivity_ = emissivity;
}

bool Radiation_0D::CalculateHeatFlux(Meteoroid *meteoroid, Atmosphere *atmos) {

    Meteoroid_0D *met_0D = static_cast<Meteoroid_0D*>(meteoroid);

    double mult = 1. / ( met_0D->cp * pow(met_0D->mass, 1./3.) * pow(met_0D->rho, 2./3.) / met_0D->shape_factor);

    met_0D->heatflux_radiation = - mult * 4 * emissivity_ * SIGMA * ( pow( met_0D->temp,4) - pow(atmos->GetTemperature(met_0D->height_m),4));

    return true;
}