//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_HEATFLUX_0D_H
#define METEOROID_ABLATION_HEATFLUX_0D_H

#include "HeatFlux.h"

class HeatFlux_0D : public HeatFlux{
public:
    bool CalculateHeatFlux(Meteoroid *meteoroid, Atmosphere *atmos);

};


#endif //METEOROID_ABLATION_HEATFLUX_0D_H
