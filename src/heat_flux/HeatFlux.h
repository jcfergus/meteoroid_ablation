//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_HEAT_FLUX_H
#define METEOROID_ABLATION_HEAT_FLUX_H

#include "../meteoroid/Meteoroid.h"
#include "../atmosphere/Atmosphere.h"

class Meteoroid;

class HeatFlux {
public:
    virtual bool CalculateHeatFlux(Meteoroid *meteoroid, Atmosphere *atmos) = 0;
};


#endif //METEOROID_ABLATION_HEAT_FLUX_H
