//
// Created by jcfergus on 10/26/22.
//

#include "HeatFlux_0D.h"
#include "../meteoroid/Meteoroid_0D.h"
#include <math.h>
#include <iostream>

bool HeatFlux_0D::CalculateHeatFlux(Meteoroid *meteoroid, Atmosphere *atmos) {

    Meteoroid_0D *met_0D = static_cast<Meteoroid_0D*>(meteoroid);

    double mult1 = 1. / ( met_0D->cp * pow(met_0D->mass, 1./3.) * pow(met_0D->rho, 2./3.) / met_0D->shape_factor);
    double simple_flux = mult1 * 0.5 * met_0D->heat_transfer_coeff * atmos->GetDensity(met_0D->height_m) * pow(met_0D->vel,3);

    double volume = met_0D->mass / met_0D->rho;
    double radius = pow(3.*volume/(4.*M_PI), 1./3.);
    double mult = 1. / ( met_0D->cp * met_0D->mass);

    met_0D->heatflux_atmos = mult * ( (M_PI/2.) * met_0D->heat_transfer_coeff* atmos->GetDensity(met_0D->height_m) * pow(met_0D->vel,3) * pow(radius,2) ) ;

    std::cout << "Atm Flux: " << simple_flux << " " << met_0D->heatflux_atmos << std::endl;

    return true;
}