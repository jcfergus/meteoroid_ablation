//
// Created by jcfergus on 10/26/22.
//

#include "Drag_0D.h"
#include "../meteoroid/Meteoroid_0D.h"
#include <math.h>
#include <iostream>

bool Drag_0D::CalculateDrag(Meteoroid *meteoroid, Atmosphere *atmos) {
    Meteoroid_0D *met_0D = static_cast<Meteoroid_0D*>(meteoroid);

    double simple_dvdt = - met_0D->shape_factor / (pow(met_0D->mass,1./3.) * pow(met_0D->rho,2./3.)) *
            atmos->GetDensity(met_0D->height_m) * met_0D->vel * met_0D->vel;
    double volume = met_0D->mass / met_0D->rho;
    double radius = pow(3.*volume/(4.*M_PI), 1./3.);

    met_0D->dvdt = - ((M_PI*atmos->GetDensity(met_0D->height_m))/(met_0D->mass)) * 
            (pow(met_0D->vel,2.) * pow(radius,2.));

    std::cout << "Acceleration: " << simple_dvdt << " " << met_0D->dvdt << std::endl;
    std::cout << "Altitude in m: " << met_0D->height_m << std::endl;

    return true;
}
