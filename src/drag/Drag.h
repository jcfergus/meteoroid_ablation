//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_Drag_H
#define METEOROID_ABLATION_Drag_H

#include "../meteoroid/Meteoroid.h"
#include "../atmosphere/Atmosphere.h"

class Meteoroid;

class Drag {
public:
    virtual bool CalculateDrag(Meteoroid *meteoroid, Atmosphere *atmos) = 0;
};


#endif //METEOROID_ABLATION_Drag_H
