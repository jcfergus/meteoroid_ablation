//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_Drag_0D_H
#define METEOROID_ABLATION_Drag_0D_H

#include "Drag.h"

class Drag_0D : public Drag{
public:
    bool CalculateDrag(Meteoroid *meteoroid, Atmosphere *atmos);

};


#endif //METEOROID_ABLATION_Drag_0D_H
