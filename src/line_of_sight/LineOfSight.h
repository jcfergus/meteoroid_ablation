//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_LINEOFSIGHT_H
#define METEOROID_ABLATION_LINEOFSIGHT_H

#include "../meteoroid/Meteoroid.h"

class Meteoroid;

class LineOfSight {
public:
    virtual bool CalculateLineOfSight(Meteoroid *meteoroid);
};


#endif //METEOROID_ABLATION_LINEOFSIGHT_H
