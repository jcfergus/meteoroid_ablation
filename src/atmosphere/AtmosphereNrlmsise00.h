//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_Atmosphere_MSISE_H
#define METEOROID_ABLATION_Atmosphere_MSISE_H

#include "Atmosphere.h"
#include <vector>

class AtmosphereMSISE : public  Atmosphere{
public:
    AtmosphereMSISE();
    double GetTemperature(double height_m);
    double GetDensity(double height_m);

    std::vector<double> temperature;
    std::vector<double> density;

};


#endif //METEOROID_ABLATION_Atmosphere_MSISE_H
