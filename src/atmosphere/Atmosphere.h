//
// Created by jcfergus on 10/26/22.
//

#ifndef METEOROID_ABLATION_ATMOSPHERE_H
#define METEOROID_ABLATION_ATMOSPHERE_H

class Meteoroid;

class Atmosphere {
public:
    virtual double GetTemperature(double height_m) = 0;
    virtual double GetDensity(double height_m) = 0;
};


#endif //METEOROID_ABLATION_ATMOSPHERE_H
