#include <iostream>
#include <vector>
#include "puma.h"
#include "meteoroid/Meteoroid_0D.h"
#include "meteoroid/Meteoroid.h"
#include "AblationSolver.h"
#include "atmosphere/AtmosphereNrlmsise00.h"
#include <math.h>
#include "utilities/Plotter.h"

using namespace std;

#define NA 6.0221408e+23

void test_1();
void test_2();
void test_3();
void test_4();
void test_5();
void test_6();
void test_original();
void test_meteoroid_0D();

int main() {

    test_meteoroid_0D();

    return 0;

}

void test_meteoroid_0D(){


    double mass_init = 1.5707963267948966e-07;
    double temp_init = 250;
    double v_init = 40000;
    double h_init = 100000;
    double angle_init = M_PI/4;
    double cp = 1200;
    double rho = 300;
    double molecular_mass = 20e-3 / NA;
    double shape_factor = 1.21;
    double heat_transfer_coeff = 1;

    Meteoroid_0D meteoroid(mass_init, temp_init, v_init, h_init, angle_init, cp, rho, molecular_mass, shape_factor, heat_transfer_coeff);

    met::AblationSolver solver(&meteoroid);
//    solver.RunSolver_nSteps(1e-4, 1);
    solver.RunSolver_MassCutoff(1e-4, 1e-11);

    Plotter::dump_data(&solver.height, &solver.temperature, "h-t");
    Plotter::dump_data(&solver.height, &solver.mass, "h-m");
    Plotter::dump_data(&solver.height, &solver.velocity, "h-U");


}


void test_original() {

//    std::vector<int> domain_sizes(7);
//    domain_sizes[0]=2000;
//    domain_sizes[1]=1500;
//    domain_sizes[2]=1000;
//    domain_sizes[3]=750;
//    domain_sizes[4]=500;
//    domain_sizes[5]=375;
//    domain_sizes[6]=250;
//
//    std::vector<double> tort_vec(7);


//    std::vector<int> domain_sizes(5);
////    domain_sizes[0]=2000;
////    domain_sizes[1]=1500;
//    domain_sizes[0]=1000;
//    domain_sizes[1]=750;
//    domain_sizes[2]=500;
//    domain_sizes[3]=375;
//    domain_sizes[4]=250;
//
//    std::vector<double> tort_vec(5);

    std::vector<int> domain_sizes(1);
//    domain_sizes[0]=2000;
//    domain_sizes[1]=1500;
    domain_sizes[0]=125;


    std::vector<double> tort_vec(1);

    for(int my_case=0;my_case<domain_sizes.size();my_case++) {
        puma::Workspace grayWS(1e-4, false);

        puma::import_3DTiff(&grayWS, "/home/jcfergus/Desktop/tiff_convergence/"+puma::PString::fromInt(domain_sizes[my_case])+".tif", 0);

        puma::Matrix<double> C;

//        puma::Vec3<double> tortuosity = compute_FVTortuosity(&grayWS, &C, puma::Cutoff(0,127), "symmetric", "conjugategradient", 'x', 1e-4, 10000, true, 0);

        puma::Vec3<double> tortuosity = puma::compute_CutCellTortuosity(&grayWS, &C, puma::Cutoff(0,127), "symmetric", "cg", 'x', 1e-5, 1000, true, 0);

        std::cout << "Tortuosity: " << tortuosity << std::endl;

        tort_vec[my_case] = tortuosity.x;

        puma::export_vtk(&C,"/home/jcfergus/Desktop/testTemperature.vtk",'b',false, 0);


//        exit(0);
    }

    for(int my_case=0;my_case<domain_sizes.size();my_case++) {
        std::cout << "Size: " << domain_sizes[my_case]<< "   tortuosity: " << tort_vec[my_case] << std::endl;
    }

}

void test_6() {
    puma::Workspace grayWS(1e-4, false);

    puma::import_3DTiff(&grayWS, "/home/jcfergus/Desktop/500_spheres.tif", 0);
    grayWS.voxelLength = 0.1/80;
    int X = grayWS.X() - 1;
    int Y = grayWS.Y() - 1;
    int Z = grayWS.Z() - 1;

//    grayWS.matrix.set(0,X, 0, Y/3.0, 0, Z, 0);
//    grayWS.matrix.set(0,X, Y/3.0, 2.0*Y/3.0, 0, Z, 255);
//    grayWS.matrix.set(0,X, 2.0*Y/3.0, Y, 0, Z, 0);

    /*
     * puma::Vec3<double> puma::compute_CutCellTortuosity(puma::Workspace *grayWS, puma::Matrix<double> *C, puma::Cutoff cutoff,
                                                   std::string sideBC, std::string solverType, char dir, double solverTol, int solverMaxIt,
                                                   bool print, int numThreads) {
     */

    puma::Matrix<double> C;

    std::cout << "here?" << std::endl;

    puma::Vec3<double> tortuosity = puma::compute_CutCellTortuosity(&grayWS, &C, puma::Cutoff(0,127), "symmetric", "cg", 'x', 1e-5, 10000, true, 1000);

    std::cout << "Tortuosity: " << tortuosity << std::endl;

    puma::export_vtk(&C,"/home/jcfergus/Desktop/testTemperature.vtk",'b',false, 0);


}


void test_5() {
    puma::Workspace grayWS(1e-4, false);

    puma::import_3DTiff(&grayWS, "/home/jcfergus/Desktop/sphere_r80.tif", 0);
    grayWS.voxelLength = 0.1/80;
    int X = grayWS.X() - 1;
    int Y = grayWS.Y() - 1;
    int Z = grayWS.Z() - 1;

//    grayWS.matrix.set(255);

    double h = grayWS.voxelLength;

    puma::Matrix<double> T(X, Y, Z, 500);
//    for (int i = 0; i < X; i++) {
//        T.set(i, i, 0, Y - 1, 0, Z - 1, 1 - (i / (X - 1.0)));
//    }
//    T.set(0,0,0,Y-1,0,Z-1,1);
//    T.set(X-1,X-1,0,Y-1,0,Z-1,0);

    puma::Matrix<double> kMatrix(X, Y, Z, 0.75);
    double rho = 2458;
    double cp = 835;

    puma::Matrix <GeometryParameters> params(X, Y, Z);
    puma::Matrix <CutCell_Geometry> geometry(X, Y, Z);

    int tri_count = 0;

    double threshold = 127.5;

    for (int i = 0; i < X; i++) {
        std::vector<double> corners(8);
        for (int j = 0; j < Y; j++) {
            for (int k = 0; k < Z; k++) {
                corners[4 * 0 + 2 * 0 + 0] = grayWS(i, j, k);
                corners[4 * 0 + 2 * 0 + 1] = grayWS(i, j, k + 1);
                corners[4 * 0 + 2 * 1 + 0] = grayWS(i, j + 1, k);
                corners[4 * 0 + 2 * 1 + 1] = grayWS(i, j + 1, k + 1);
                corners[4 * 1 + 2 * 0 + 0] = grayWS(i + 1, j, k);
                corners[4 * 1 + 2 * 0 + 1] = grayWS(i + 1, j, k + 1);
                corners[4 * 1 + 2 * 1 + 0] = grayWS(i + 1, j + 1, k);
                corners[4 * 1 + 2 * 1 + 1] = grayWS(i + 1, j + 1, k + 1);

                if (corners[4 * 0 + 2 * 0 + 0] < threshold &&
                    corners[4 * 0 + 2 * 0 + 1] < threshold &&
                    corners[4 * 0 + 2 * 1 + 0] < threshold &&
                    corners[4 * 0 + 2 * 1 + 1] < threshold &&
                    corners[4 * 1 + 2 * 0 + 0] < threshold &&
                    corners[4 * 1 + 2 * 0 + 1] < threshold &&
                    corners[4 * 1 + 2 * 1 + 0] < threshold &&
                    corners[4 * 1 + 2 * 1 + 1] < threshold) {
                    T(i, j, k) = -0;
                }

                CutCell_Geometry geom = CutCell_Geometry_Generator::get_geometry(&corners, threshold);

                tri_count += geom.tris.size();

                geometry(i, j, k) = geom;

                GeometryParameters param_0;
                param_0.lambda = 0.75;
                param_0.mass = geom.volume * h * h * h * rho;
                param_0.area_xp = geom.area_xp * h * h;
                param_0.area_xm = geom.area_xm * h * h;
                param_0.area_yp = geom.area_yp * h * h;
                param_0.area_ym = geom.area_ym * h * h;
                param_0.area_zp = geom.area_zp * h * h;
                param_0.area_zm = geom.area_zm * h * h;
                param_0.area_internal = geom.area_internal * h * h;
                param_0.h_xp = geom.h_xp * h;
                param_0.h_xm = geom.h_xm * h;
                param_0.h_yp = geom.h_yp * h;
                param_0.h_ym = geom.h_ym * h;
                param_0.h_zp = geom.h_zp * h;
                param_0.h_zm = geom.h_zm * h;
                param_0.cp = cp;

                if (geom.h_xp == 0 || geom.h_xm == 0 || geom.h_yp == 0 || geom.h_ym == 0 || geom.h_zp == 0 ||
                    geom.h_zm == 0) {
                    std::cout << "this is where the problem is" << std::endl;
                }

                if (geom.h_xp != geom.h_xp || geom.h_xm != geom.h_xm || geom.h_yp != geom.h_yp ||
                    geom.h_ym != geom.h_ym || geom.h_zp != geom.h_zp || geom.h_zm != geom.h_zm) {
                    std::cout << "this is where the problem is - nanwhy" << std::endl;
                }

                params(i, j, k) = param_0;

            }
        }
    }
//    std::vector<puma::Triangle < float> > triangles;
//    triangles.reserve(tri_count);
//
//    for (int i = 0; i < X; i++) {
//        for (int j = 0; j < Y; j++) {
//            for (int k = 0; k < Z; k++) {
//
//                std::vector<puma::Triangle < float> > my_triangles = geometry(i, j, k).tris;
//                for (int t = 0; t < my_triangles.size(); t++) {
//                    my_triangles[t].p0.x += i;
//                    my_triangles[t].p1.x += i;
//                    my_triangles[t].p2.x += i;
//                    my_triangles[t].p0.y += j;
//                    my_triangles[t].p1.y += j;
//                    my_triangles[t].p2.y += j;
//                    my_triangles[t].p0.z += k;
//                    my_triangles[t].p1.z += k;
//                    my_triangles[t].p2.z += k;
//                }
//
//                triangles.insert(triangles.end(), my_triangles.begin(), my_triangles.end());
//            }
//        }
//    }

//    puma::export_STL(&triangles, false, "/home/jcfergus/Desktop/cutcell_stl.stl");

    double accuracy = 1e-5;
    double timeStep = 10;
    double numThreads = 8;
    bool print = true;

    puma::CutCell_TransientSolver cutcell_solver(&T, &params, accuracy, timeStep, numThreads, print);

    double h_coeff = 30;
    double Tref = 20;
    puma::Matrix<double> heat_flux(X, Y, Z, 0);

    for(int n=0;n<500;n++) {
        heat_flux.set(0);

        for (int i = 0; i < X; i++) {
            for (int j = 0; j < Y; j++) {
                for (int k = 0; k < Z; k++) {
                    if(params(i, j, k).area_internal != 0) {
                        heat_flux(i,j,k) = h_coeff * (T(i,j,k) - Tref) * params(i, j, k).area_internal;
//                          heat_flux(i,j,k) = params(i,j,k).mass * params(i,j,k).cp * 1;
//                        std::cout << heat_flux(i,j,k) << std::endl;
                    }
//                    heat_flux(i,j,k) = 1;
//                    std::cout << params(i,j,k).mass << " " << heat_flux(i,j,k)  << std::endl;
//                    exit(0);
                }
            }
        }

//        puma::export_vtk(&heat_flux,"/home/jcfergus/Desktop/testheatflux.vtk",'b',false, 0);
//        exit(0);

        cutcell_solver.set_heat_flux(&heat_flux);
        cutcell_solver.run_solver(1, true, "/home/jcfergus/Desktop/vtk10", 1, n);

        std::cout << n << " " << T.average() << std::endl;

    }


}

void test_4() {
    puma::Workspace grayWS(1e-4, false);

    puma::import_3DTiff(&grayWS, "/home/jcfergus/Desktop/sphere_r20.tif", 0);
    grayWS.voxelLength = 0.005;
    int X = grayWS.X() - 1;
    int Y = grayWS.Y() - 1;
    int Z = grayWS.Z() - 1;

    double h = grayWS.voxelLength;

    puma::Matrix<double> T(X, Y, Z, 500);
//    for (int i = 0; i < X; i++) {
//        T.set(i, i, 0, Y - 1, 0, Z - 1, 1 - (i / (X - 1.0)));
//    }
//    T.set(0,0,0,Y-1,0,Z-1,1);
//    T.set(X-1,X-1,0,Y-1,0,Z-1,0);

    puma::Matrix<double> kMatrix(X, Y, Z, 0.75);
    double rho = 2458;
    double cp = 835;

    puma::Matrix <GeometryParameters> params(X, Y, Z);
    puma::Matrix <CutCell_Geometry> geometry(X, Y, Z);

    int tri_count = 0;

    double threshold = 127.5;

    for (int i = 0; i < X; i++) {
        std::vector<double> corners(8);
        for (int j = 0; j < Y; j++) {
            for (int k = 0; k < Z; k++) {
                corners[4 * 0 + 2 * 0 + 0] = grayWS(i, j, k);
                corners[4 * 0 + 2 * 0 + 1] = grayWS(i, j, k + 1);
                corners[4 * 0 + 2 * 1 + 0] = grayWS(i, j + 1, k);
                corners[4 * 0 + 2 * 1 + 1] = grayWS(i, j + 1, k + 1);
                corners[4 * 1 + 2 * 0 + 0] = grayWS(i + 1, j, k);
                corners[4 * 1 + 2 * 0 + 1] = grayWS(i + 1, j, k + 1);
                corners[4 * 1 + 2 * 1 + 0] = grayWS(i + 1, j + 1, k);
                corners[4 * 1 + 2 * 1 + 1] = grayWS(i + 1, j + 1, k + 1);

                if (corners[4 * 0 + 2 * 0 + 0] < threshold &&
                    corners[4 * 0 + 2 * 0 + 1] < threshold &&
                    corners[4 * 0 + 2 * 1 + 0] < threshold &&
                    corners[4 * 0 + 2 * 1 + 1] < threshold &&
                    corners[4 * 1 + 2 * 0 + 0] < threshold &&
                    corners[4 * 1 + 2 * 0 + 1] < threshold &&
                    corners[4 * 1 + 2 * 1 + 0] < threshold &&
                    corners[4 * 1 + 2 * 1 + 1] < threshold) {
                    T(i, j, k) = -0;
                }

                CutCell_Geometry geom = CutCell_Geometry_Generator::get_geometry(&corners, threshold);

                tri_count += geom.tris.size();

                geometry(i, j, k) = geom;

                GeometryParameters param_0;
                param_0.lambda = 75;
                param_0.mass = geom.volume * h * h * h * rho;
                param_0.area_xp = geom.area_xp * h * h;
                param_0.area_xm = geom.area_xm * h * h;
                param_0.area_yp = geom.area_yp * h * h;
                param_0.area_ym = geom.area_ym * h * h;
                param_0.area_zp = geom.area_zp * h * h;
                param_0.area_zm = geom.area_zm * h * h;
                param_0.area_internal = geom.area_internal * h * h;
                param_0.h_xp = geom.h_xp * h;
                param_0.h_xm = geom.h_xm * h;
                param_0.h_yp = geom.h_yp * h;
                param_0.h_ym = geom.h_ym * h;
                param_0.h_zp = geom.h_zp * h;
                param_0.h_zm = geom.h_zm * h;
                param_0.cp = cp;

                if (geom.h_xp == 0 || geom.h_xm == 0 || geom.h_yp == 0 || geom.h_ym == 0 || geom.h_zp == 0 ||
                    geom.h_zm == 0) {
                    std::cout << "this is where the problem is" << std::endl;
                }

                if (geom.h_xp != geom.h_xp || geom.h_xm != geom.h_xm || geom.h_yp != geom.h_yp ||
                    geom.h_ym != geom.h_ym || geom.h_zp != geom.h_zp || geom.h_zm != geom.h_zm) {
                    std::cout << "this is where the problem is - nanwhy" << std::endl;
                }

                params(i, j, k) = param_0;

            }
        }
    }
    std::vector<puma::Triangle < float> > triangles;
    triangles.reserve(tri_count);

    for (int i = 0; i < X; i++) {
        for (int j = 0; j < Y; j++) {
            for (int k = 0; k < Z; k++) {

                std::vector<puma::Triangle < float> > my_triangles = geometry(i, j, k).tris;
                for (int t = 0; t < my_triangles.size(); t++) {
                    my_triangles[t].p0.x += i;
                    my_triangles[t].p1.x += i;
                    my_triangles[t].p2.x += i;
                    my_triangles[t].p0.y += j;
                    my_triangles[t].p1.y += j;
                    my_triangles[t].p2.y += j;
                    my_triangles[t].p0.z += k;
                    my_triangles[t].p1.z += k;
                    my_triangles[t].p2.z += k;
                }

                triangles.insert(triangles.end(), my_triangles.begin(), my_triangles.end());
            }
        }
    }

    puma::export_STL(&triangles, false, "/home/jcfergus/Desktop/cutcell_stl.stl");

    double accuracy = 1e-5;
    double timeStep = 0.1;
    double numThreads = 8;
    bool print = true;

    puma::CutCell_TransientSolver cutcell_solver(&T, &params, accuracy, timeStep, numThreads, print);

    double h_coeff = 30;
    double Tref = 20;
    puma::Matrix<double> heat_flux(X, Y, Z, 0);

    for(int n=0;n<100;n++) {
        heat_flux.set(0);

        for (int i = 0; i < X; i++) {
            for (int j = 0; j < Y; j++) {
                for (int k = 0; k < Z; k++) {
                    if(params(i, j, k).area_internal != 0) {
                        heat_flux(i,j,k) = h_coeff * (T(i,j,k) - Tref) * params(i, j, k).area_internal;
//                        std::cout << heat_flux(i,j,k) << std::endl;
                    }
                }
            }
        }

//        puma::export_vtk(&heat_flux,"/home/jcfergus/Desktop/testheatflux.vtk",'b',false, 0);
//        exit(0);

        cutcell_solver.set_heat_flux(&heat_flux);
        cutcell_solver.run_solver(1, true, "/home/jcfergus/Desktop/vtkfolder2", 1, n);

    }


}

void test_3(){
    puma::Workspace grayWS(1e-4, false);

    puma::import_3DTiff(&grayWS,"/home/jcfergus/Desktop/circle_tif.tif",0);
    grayWS.voxelLength = 1e-4;
    int X = grayWS.X()-1;
    int Y = grayWS.Y()-1;
    int Z = grayWS.Z()-1;

    double h = grayWS.voxelLength;

    puma::Matrix<double> T(X, Y, Z, 0);
    for(int i=0;i<X;i++) {
        T.set(i,i,0,Y-1,0,Z-1,1-(i/(X-1.0)));
    }
//    T.set(0,0,0,Y-1,0,Z-1,1);
//    T.set(X-1,X-1,0,Y-1,0,Z-1,0);

    puma::Matrix<double> kMatrix(X,Y,Z, 1);
    puma::Matrix<double> rhoCp(X,Y,Z,1);

    puma::Matrix<GeometryParameters> params(X,Y,Z);
    puma::Matrix<CutCell_Geometry> geometry(X,Y,Z);

    int tri_count = 0;

    double threshold = 127.5;

    for(int i=0;i<X;i++){
        std::vector<double> corners(8);
        for(int j=0;j<Y;j++){
            for(int k=0;k<Z;k++){
                corners[4 * 0 + 2 * 0 + 0] = grayWS(i,j,k);
                corners[4 * 0 + 2 * 0 + 1] = grayWS(i,j,k+1);
                corners[4 * 0 + 2 * 1 + 0] = grayWS(i,j+1,k);
                corners[4 * 0 + 2 * 1 + 1] = grayWS(i,j+1,k+1);
                corners[4 * 1 + 2 * 0 + 0] = grayWS(i+1,j,k);
                corners[4 * 1 + 2 * 0 + 1] = grayWS(i+1,j,k+1);
                corners[4 * 1 + 2 * 1 + 0] = grayWS(i+1,j+1,k);
                corners[4 * 1 + 2 * 1 + 1] = grayWS(i+1,j+1,k+1);

                if(corners[4 * 0 + 2 * 0 + 0] < threshold &&
                   corners[4 * 0 + 2 * 0 + 1] < threshold &&
                   corners[4 * 0 + 2 * 1 + 0] < threshold &&
                   corners[4 * 0 + 2 * 1 + 1] < threshold &&
                   corners[4 * 1 + 2 * 0 + 0] < threshold &&
                   corners[4 * 1 + 2 * 0 + 1] < threshold &&
                   corners[4 * 1 + 2 * 1 + 0] < threshold &&
                   corners[4 * 1 + 2 * 1 + 1] < threshold ) {
                    T(i,j,k) = -0;
                }


                CutCell_Geometry geom = CutCell_Geometry_Generator::get_geometry(&corners, threshold);

                tri_count += geom.tris.size();

                geometry(i,j,k) = geom;

                GeometryParameters param_0;
                param_0.lambda = 1;
                param_0.mass = geom.volume * h*h*h * 1;
                param_0.area_xp = geom.area_xp * h*h;
                param_0.area_xm = geom.area_xm * h*h;
                param_0.area_yp = geom.area_yp * h*h;
                param_0.area_ym = geom.area_ym * h*h;
                param_0.area_zp = geom.area_zp * h*h;
                param_0.area_zm = geom.area_zm * h*h;
                param_0.h_xp = geom.h_xp * h;
                param_0.h_xm = geom.h_xm * h;
                param_0.h_yp = geom.h_yp * h;
                param_0.h_ym = geom.h_ym * h;
                param_0.h_zp = geom.h_zp * h;
                param_0.h_zm = geom.h_zm * h;
                param_0.cp = 1;

                if(geom.h_xp == 0 || geom.h_xm == 0 || geom.h_yp == 0 || geom.h_ym == 0 || geom.h_zp == 0 || geom.h_zm == 0) {
                    std::cout << "this is where the problem is" << std::endl;
                }

                if(geom.h_xp != geom.h_xp || geom.h_xm != geom.h_xm || geom.h_yp != geom.h_yp || geom.h_ym != geom.h_ym || geom.h_zp != geom.h_zp || geom.h_zm != geom.h_zm) {
                    std::cout << "this is where the problem is - nanwhy" << std::endl;
                }

                params(i,j,k) = param_0;

            }
        }
    }
    std::vector< puma::Triangle<float> > triangles;
    triangles.reserve(tri_count);

    for(int i=0;i<X;i++){
        for(int j=0;j<Y;j++) {
            for (int k = 0; k < Z; k++) {

                std::vector< puma::Triangle<float> > my_triangles = geometry(i,j,k).tris;
                for(int t=0;t<my_triangles.size(); t++){
                    my_triangles[t].p0.x += i;
                    my_triangles[t].p1.x += i;
                    my_triangles[t].p2.x += i;
                    my_triangles[t].p0.y += j;
                    my_triangles[t].p1.y += j;
                    my_triangles[t].p2.y += j;
                    my_triangles[t].p0.z += k;
                    my_triangles[t].p1.z += k;
                    my_triangles[t].p2.z += k;
                }

                triangles.insert(triangles.end(), my_triangles.begin(), my_triangles.end());
            }
        }
    }

    puma::export_STL(&triangles, false, "/home/jcfergus/Desktop/cutcell_stl.stl");

    double accuracy = 1e-6;
    double timeStep = 1e-6;
    double numThreads = 8;
    bool print = true;

    puma::CutCell_TransientSolver cutcell_solver(&T, &params, accuracy, timeStep, numThreads, print);

    cutcell_solver.run_solver(500, true, "/home/jcfergus/Desktop/vtkfolder2", 1, 0);



}

void test_2(){
    puma::Workspace grayWS(100,100,100, 1e-4, false);

    puma::import_3DTiff(&grayWS,"/home/jcfergus/Desktop/2dtiff.tif",0);
    int X = grayWS.X()-1;
    int Y = grayWS.Y()-1;
    int Z = grayWS.Z()-1;

//    grayWS.matrix.set(0,X, 0, Y, 0, Z, 0);
//    grayWS.matrix.set(0,X, Y/3.0, 2.0*Y/3.0, Z/3.0, 2.0*Z/3.0, 100);
//    grayWS.matrix.set(0,X, 2.0*Y / 3.0, Y, 0, Z, 0);
//    grayWS.matrix.set(0, 90, 0, 190, 0, Z, 0);
//    grayWS.matrix.set(110, X, 10, Y, 0, Z, 0);

//    for(int i=0;i<X+1;i++) {
//        for(int j=0;j<Y+1;j++) {
//            grayWS.matrix(i,j,1) = grayWS.matrix(i,j,0);
//        }
//    }



    for(int i=0;i<grayWS.size();i++) {
        grayWS.matrix(i) = 255 - grayWS.matrix(i);
    }


    double h = grayWS.voxelLength;

    puma::Matrix<double> T(X, Y, Z, 0);
//    for(int i=0;i<X;i++) {
//        T.set(i,i,0,Y-1,0,Z-1,1-(i/(X-1.0)));
//    }
    T.set(0,0,0,Y-1,0,Z-1,1);
    T.set(X-1,X-1,0,Y-1,0,Z-1,0);

    puma::Matrix<double> kMatrix(X,Y,Z, 1);
    puma::Matrix<double> rhoCp(X,Y,Z,1);

    puma::Matrix<GeometryParameters> params(X,Y,Z);
    puma::Matrix<CutCell_Geometry> geometry(X,Y,Z);

    int tri_count = 0;

    double threshold = 10.5;

    for(int i=0;i<X;i++){
        std::vector<double> corners(8);
        for(int j=0;j<Y;j++){
            for(int k=0;k<Z;k++){
                corners[4 * 0 + 2 * 0 + 0] = grayWS(i,j,k);
                corners[4 * 0 + 2 * 0 + 1] = grayWS(i,j,k+1);
                corners[4 * 0 + 2 * 1 + 0] = grayWS(i,j+1,k);
                corners[4 * 0 + 2 * 1 + 1] = grayWS(i,j+1,k+1);
                corners[4 * 1 + 2 * 0 + 0] = grayWS(i+1,j,k);
                corners[4 * 1 + 2 * 0 + 1] = grayWS(i+1,j,k+1);
                corners[4 * 1 + 2 * 1 + 0] = grayWS(i+1,j+1,k);
                corners[4 * 1 + 2 * 1 + 1] = grayWS(i+1,j+1,k+1);

                if(corners[4 * 0 + 2 * 0 + 0] < threshold &&
                        corners[4 * 0 + 2 * 0 + 1] < threshold &&
                        corners[4 * 0 + 2 * 1 + 0] < threshold &&
                        corners[4 * 0 + 2 * 1 + 1] < threshold &&
                        corners[4 * 1 + 2 * 0 + 0] < threshold &&
                        corners[4 * 1 + 2 * 0 + 1] < threshold &&
                        corners[4 * 1 + 2 * 1 + 0] < threshold &&
                        corners[4 * 1 + 2 * 1 + 1] < threshold ) {
                    T(i,j,k) = -0;
                }


                CutCell_Geometry geom = CutCell_Geometry_Generator::get_geometry(&corners, threshold);

                tri_count += geom.tris.size();

                geometry(i,j,k) = geom;

                /*
                 *         return_vals.volume = 1;
        return_vals.area_xm = 1;
        return_vals.area_xp = 1;
        return_vals.area_ym = 1;
        return_vals.area_yp = 1;
        return_vals.area_zm = 1;
        return_vals.area_zp = 1;
        return_vals.h_xm = 0.5;
        return_vals.h_xp = 0.5;
        return_vals.h_ym = 0.5;
        return_vals.h_yp = 0.5;
        return_vals.h_zm = 0.5;
        return_vals.h_zp = 0.5;
                 */

                GeometryParameters param_0;
                param_0.lambda = 1;
                param_0.mass = geom.volume * h*h*h * 1;
                param_0.area_xp = geom.area_xp * h*h;
                param_0.area_xm = geom.area_xm * h*h;
                param_0.area_yp = geom.area_yp * h*h;
                param_0.area_ym = geom.area_ym * h*h;
                param_0.area_zp = geom.area_zp * h*h;
                param_0.area_zm = geom.area_zm * h*h;
                param_0.h_xp = geom.h_xp * h;
                param_0.h_xm = geom.h_xm * h;
                param_0.h_yp = geom.h_yp * h;
                param_0.h_ym = geom.h_ym * h;
                param_0.h_zp = geom.h_zp * h;
                param_0.h_zm = geom.h_zm * h;
                param_0.cp = 1;

                if(geom.h_xp == 0 || geom.h_xm == 0 || geom.h_yp == 0 || geom.h_ym == 0 || geom.h_zp == 0 || geom.h_zm == 0) {
                    std::cout << "this is where the problem is" << std::endl;
                }

                if(geom.h_xp != geom.h_xp || geom.h_xm != geom.h_xm || geom.h_yp != geom.h_yp || geom.h_ym != geom.h_ym || geom.h_zp != geom.h_zp || geom.h_zm != geom.h_zm) {
                    std::cout << "this is where the problem is - nanwhy" << std::endl;
                }

                params(i,j,k) = param_0;

            }
        }
    }


    /*
     * AB.reserve( A.size() + B.size() ); // preallocate memory
AB.insert( AB.end(), A.begin(), A.end() );
AB.insert( AB.end(), B.begin(), B.end() );
     */

    std::vector< puma::Triangle<float> > triangles;
    triangles.reserve(tri_count);

    //bool puma::export_STL(std::vector< puma::Triangle<float> > *triangles, bool Ascii, std::string fileName)
    for(int i=0;i<X;i++){
        for(int j=0;j<Y;j++) {
            for (int k = 0; k < Z; k++) {

                std::vector< puma::Triangle<float> > my_triangles = geometry(i,j,k).tris;
                for(int t=0;t<my_triangles.size(); t++){
                    my_triangles[t].p0.x += i;
                    my_triangles[t].p1.x += i;
                    my_triangles[t].p2.x += i;
                    my_triangles[t].p0.y += j;
                    my_triangles[t].p1.y += j;
                    my_triangles[t].p2.y += j;
                    my_triangles[t].p0.z += k;
                    my_triangles[t].p1.z += k;
                    my_triangles[t].p2.z += k;
                }

                triangles.insert(triangles.end(), my_triangles.begin(), my_triangles.end());
            }
        }
    }

    puma::export_STL(&triangles, false, "/home/jcfergus/Desktop/cutcell_stl.stl");



    double accuracy = 1e-6;
    double timeStep = 1e-6;
    double numThreads = 0;
    bool print = true;

    puma::CutCell_TransientSolver cutcell_solver(&T, &params, accuracy, timeStep, numThreads, print);

    cutcell_solver.run_solver(500, true, "/home/jcfergus/Desktop/vtkfolder2", 1, 0);



}


void test_1(){

    // Creates an empty workspace
    puma::Workspace grayWS(500,5,2,1e-6, false);
//    // Preparing inputs for fibers generation
//    RandomFibersInput input;
//    /*
//     * int xSize, int ySize, int zSize, double avgRadius, double dRadius,
//                        double avgLength, double dLength, int angle_type, double angle_variability, int var_direction,
//                        bool intersect, double poro, int randomSeed
//     */
//    input.straightCircle(200,200,10,5,0,10,0,0,0,0,true,0.6,100);
//    input.print = true; // printing option can be turned off like this, ON as a default
//    puma::generateRandomFibers(&grayWS, input);    // Generating fibers

//    grayWS.matrix.set(0,49,0,9,0,9,0);
//    grayWS.matrix.set(50,99,0,9,0,9,255);


    // Initializing Temperature field and material conductivity
    puma::Matrix<double> T(grayWS.X(),grayWS.Y(),grayWS.Z());
    for(int i=0;i<grayWS.X();i++) {
        double h = 1./grayWS.X();
        double Ti = 0.5*h + i * h;
        for(int j=0;j<grayWS.Y();j++) {
            for(int k=0;k<grayWS.Z();k++) {
                T(i,j,k) = 0;
            }
        }
    }

    T.set(0,100,0,4,0,1,1);
    T.set(400,499,0,4,0,1,1);

    puma::Matrix<double> kMatrix(grayWS.X(),grayWS.Y(),grayWS.Z());
    for(int i=0;i<grayWS.X();i++) {
        for(int j=0;j<grayWS.Y();j++) {
            for(int k=0;k<grayWS.Z();k++) {
                if(grayWS(i,j,k)<128) {
                    kMatrix(i,j,k)=1;
                } else{
                    kMatrix(i,j,k) = 12;
                }
            }
        }
    }

//    kMatrix.set(0,0,0,199,0,9,0);
//    kMatrix.set(199,199,0,199,0,9,0);

    puma::Matrix<double> rhoCp(grayWS.X(),grayWS.Y(),grayWS.Z(),1);

    double accuracy = 1e-6;
    double timeStep = 1e-2;
    double h = 1.0/grayWS.X();
    double numThreads = 0;
    bool print = true;


    std::cout << T.average() << std::endl;

//    puma::TransientSolver my_solver(&kMatrix, &T, &rhoCp, accuracy, timeStep,  h, numThreads, print);
//    my_solver.setup();
//    my_solver.run_solver(100, true, "/home/jcfergus/Desktop/vtkfolder/", 1);

    double rho = 1;
    double volume = h * h * h;
    double area = h * h;



    puma::Matrix<GeometryParameters> params(grayWS.X(),grayWS.Y(),grayWS.Z());


    GeometryParameters param_0;
    param_0.lambda = 1;
    param_0.mass = 0;
    param_0.area_xp = 0;
    param_0.area_xm = 0;
    param_0.area_yp = 0;
    param_0.area_ym = 0;
    param_0.area_zp = 0;
    param_0.area_zm = 0;
    param_0.h_xp = 1 * h;
    param_0.h_xm = 1 * h;
    param_0.h_yp = 1 * h;
    param_0.h_ym = 1 * h;
    param_0.h_zp = 1 * h;
    param_0.h_zm = 1 * h;
    param_0.cp = 1;

    GeometryParameters param_1;
    param_1.lambda = 1;
    param_1.mass = rho * volume * 0.5;
    param_1.area_xp = area * 0.5;
    param_1.area_xm = area * 0.5;
    param_1.area_yp = area;
    param_1.area_ym = 0;
    param_1.area_zp = area;
    param_1.area_zm = area;
    param_1.h_xp = 0.5 * h;
    param_1.h_xm = 0.5 * h;
    param_1.h_yp = 0.75 * h;
    param_1.h_ym = 0.25 * h;
    param_1.h_zp = 0.5 * h;
    param_1.h_zm = 0.5 * h;
    param_1.cp = 1;

    GeometryParameters param_2;
    param_2.lambda = 1;
    param_2.mass = rho * volume;
    param_2.area_xp = area;
    param_2.area_xm = area;
    param_2.area_yp = area;
    param_2.area_ym = area;
    param_2.area_zp = area;
    param_2.area_zm = area;
    param_2.h_xp = 0.5 * h;
    param_2.h_xm = 0.5 * h;
    param_2.h_yp = 0.5 * h;
    param_2.h_ym = 0.5 * h;
    param_2.h_zp = 0.5 * h;
    param_2.h_zm = 0.5 * h;
    param_2.cp = 1;

    GeometryParameters param_3;
    param_3.lambda = 1;
    param_3.mass = rho * volume * 0.5;
    param_3.area_xp = area * 0.5;
    param_3.area_xm = area * 0.5;
    param_3.area_yp = 0;
    param_3.area_ym = area;
    param_3.area_zp = area;
    param_3.area_zm = area;
    param_3.h_xp = 0.5 * h;
    param_3.h_xm = 0.5 * h;
    param_3.h_yp = 0.25 * h;
    param_3.h_ym = 0.75 * h;
    param_3.h_zp = 0.5 * h;
    param_3.h_zm = 0.5 * h;
    param_3.cp = 1;

    GeometryParameters param_4;
    param_4.lambda = 1;
    param_4.mass = 0;
    param_4.area_xp = 0;
    param_4.area_xm = 0;
    param_4.area_yp = 0;
    param_4.area_ym = 0;
    param_4.area_zp = 0;
    param_4.area_zm = 0;
    param_4.h_xp = 1 * h;
    param_4.h_xm = 1 * h;
    param_4.h_yp = 1 * h;
    param_4.h_ym = 1 * h;
    param_4.h_zp = 1 * h;
    param_4.h_zm = 1 * h;
    param_4.cp = 1;

    params.set(0,params.X()-1, 0, 0, 0, params.Z()-1, param_0);
    params.set(0,params.X()-1, 1, 1, 0, params.Z()-1, param_1);
    params.set(0,params.X()-1, 2, 2, 0, params.Z()-1, param_2);
    params.set(0,params.X()-1, 3, 3, 0, params.Z()-1, param_3);
    params.set(0,params.X()-1, 4, 4, 0, params.Z()-1, param_4);

    std::cout << params(0,0,0).mass << " " << params(0,1,0).mass << " " << params(0,2,0).mass << " " << params(0,3,0).mass << " " << params(0,4,0).mass << std::endl;

//CutCell_TransientSolver(puma::Matrix<double> *T, puma::Matrix<GeometryParameters> *geom_params, double accuracy, double timeStep, int numThreads, bool print);
//

    puma::CutCell_TransientSolver cutcell_solver(&T, &params, accuracy, timeStep, numThreads, print);

    cutcell_solver.run_solver(100, true, "/home/jcfergus/Desktop/vtkfolder_cutcell", 1, 0);

}

//
//    puma::Matrix<double> T;
//
//
//
//    puma::TransientSolver my_solver(puma::Matrix<double> *kMatrix, puma::Matrix<double> *T, puma::Matrix<double> *rhoCp, double accuracy, double timeStep,  double h, int numThreads, bool print) {
//
//
//        bool TransientSolver::run_solver(int numTimeSteps, bool output_steps, std::string outfolder, int output_iteration){
//
//
//            cout << endl << "Conductivity: " << endl;
//    cout << "kxx " << kx.x << " kxy " << kx.y << " kxz " << kx.z << endl;
//    cout << "kyx " << ky.x << " kyy " << ky.y << " kyz " << ky.z << endl;
//    cout << "kzx " << kz.x << " kzy " << kz.y << " kzz " << kz.z << endl;
//
//    return 0;

//}
