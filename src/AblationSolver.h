#ifndef METEOROID_ABLATION_SOLVER_H
#define METEOROID_ABLATION_SOLVER_H

#include "meteoroid/Meteoroid.h"
#include <vector>

namespace met{

    class AblationSolver {
    public:
        AblationSolver(Meteoroid *meteoroid);
        bool RunSolver_nSteps(double dt, int num_steps);
        bool RunSolver_MassCutoff(double dt, double mass_minimum);
        bool RunTimeStep(double dt);

        std::vector<double> mass;
        std::vector<double> velocity;
        std::vector<double> height;
        std::vector<double> temperature;

    private:
        Meteoroid *meteoroid;
    };

}





#endif //METEOROID_ABLATION_SOLVER_H
