cd "${0%/*}" || exit 1    # Run from this directory
set -e  # exit when any command fails

eval "$(conda shell.bash hook)"  # this is required for conda
conda activate plot

python plot.py


