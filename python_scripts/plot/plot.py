import matplotlib.pyplot as plt
import numpy as np
import csv

# Temperature Plots
# Open files from new formulations
x_file = "h-tx.txt"
y_file = "h-ty.txt"

with open(x_file, 'r') as file:
       data = file.read()
       x = [float(i) for i in data.split(',')[:-1]]

with open(y_file, 'r') as file:
       data = file.read()
       y = [float(i) for i in data.split(',')[:-1]]

# Open files from legacy model
x_file = "OG_h-tx.txt"
y_file = "OG_h-ty.txt"

with open(x_file, 'r') as file:
       data = file.read()
       x0D = [float(i) for i in data.split(',')[:-1]]

with open(y_file, 'r') as file:
       data = file.read()
       y0D = [float(i) for i in data.split(',')[:-1]]

#Temperature Plot
fig, ax = plt.subplots()
ax.plot(np.divide(x,1000), y, label = 'S&C Equations')
ax.plot(np.divide(x0D,1000), y0D, label = 'Legacy Model')
ax.set(xlabel='Altitude (km)', ylabel='Temperature (K)')
ax.grid()
ax.legend()
fig.savefig('compare_temp.png')

# Mass Plots
# Open files from new formulations
x_file = "h-mx.txt"
y_file = "h-my.txt"

with open(x_file, 'r') as file:
       data = file.read()
       x = [float(i) for i in data.split(',')[:-1]]

with open(y_file, 'r') as file:
       data = file.read()
       y = [float(i) for i in data.split(',')[:-1]]

# Open files from legacy model
x_file = "OG_h-mx.txt"
y_file = "OG_h-my.txt"

with open(x_file, 'r') as file:
       data = file.read()
       x0D = [float(i) for i in data.split(',')[:-1]]

with open(y_file, 'r') as file:
       data = file.read()
       y0D = [float(i) for i in data.split(',')[:-1]]

# Mass Plot
fig, ax = plt.subplots()
ax.plot(np.divide(x,1000), y, label = 'S&C Equations')
ax.plot(np.divide(x0D,1000), y0D, label = 'Legacy Model')
ax.set(xlabel='Altitude (km)', ylabel='Mass (kg)')
ax.grid()
ax.legend()
fig.savefig('compare_mass.png')

def dm_dt_calc(mass, time):
       dm_array = np.divide(mass,time)
       return dm_array

#Retrieve time
dt = 1e-4
time_array = [i*dt for i in range(len(x))]

#Plot mass vs time
fig, ax = plt.subplots()
ax.plot(time_array, y, label = 'S&C Equations')
ax.set(xlabel='time (s)', ylabel='Mass (kg)')
ax.grid()
ax.legend()
fig.savefig('mass_vs_time.png')

#Plot mass rate of change vs time
dm_dt = np.diff(y)/dt
fig, ax = plt.subplots()
ax.plot(time_array[:-1], dm_dt, label = 'S&C Equations')
ax.set(xlabel='time (s)', ylabel='Rate of Change of Mass (kg/s)')
ax.grid()
ax.legend()
fig.savefig('der_mass_vs_time.png')

#Plot mass rate of change vs altitude
dm_dt = np.diff(y)/dt
fig, ax = plt.subplots()
ax.plot(np.divide(x[:-1],1000), dm_dt, label = 'S&C Equations')
ax.set(xlabel='Altitude (km)', ylabel='Rate of Change of Mass (kg/s)')
ax.grid()
ax.legend()
fig.savefig('der_mass_vs_alt.png')


# Retrieve velocity data from new formulations
x_file = "h-Ux.txt"
y_file = "h-Uy.txt"

with open(x_file, 'r') as file:
       data = file.read()
       x = [float(i) for i in data.split(',')[:-1]]

with open(y_file, 'r') as file:
       data = file.read()
       y = [float(i) for i in data.split(',')[:-1]]


# Calculate acceleration and plot it vs time
dU_dt = np.diff(y)/dt
fig, ax = plt.subplots()
ax.plot(time_array[:-1], np.divide(dU_dt,1000**2))
ax.set(xlabel='Time (s)', ylabel='Acceleration (km/s$^2$)')
ax.grid()
fig.tight_layout()
fig.savefig('der_U_vs_time.png')

# Plot velocity vs time
fig, ax = plt.subplots()
ax.plot(time_array, np.divide(y,1000))
ax.set(xlabel='Time (s)', ylabel='Velocity (km/s)')
ax.grid()
fig.tight_layout()
fig.savefig('U_vs_time.png')

# Acceleration vs altitude
fig, ax = plt.subplots()
ax.plot(np.divide(x[:-1],1000), np.divide(dU_dt,1000**2))
ax.set(xlabel='Altitude (km)', ylabel='Acceleration (km/s$^2$)')
ax.grid()
fig.tight_layout()
fig.savefig('der_U_vs_alt.png')

# Plot velocity vs time
fig, ax = plt.subplots()
ax.plot(np.divide(x,1000), np.divide(y,1000))
ax.set(xlabel='Altitude (km)', ylabel='Velocity (km/s)')
ax.grid()
fig.tight_layout()
fig.savefig('U_vs_alt.png')