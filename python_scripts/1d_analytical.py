import numpy as np
import matplotlib.pyplot as plt

def plot_analytical(L, dx, time, steps, alpha):

    nx = int(L / dx) + 1

    x = np.linspace(0,1,nx)

    print(nx, x.shape)

    yt = np.zeros((steps,nx))
    time = np.linspace(0,time, steps)

    for k in range(steps):
        for n in range(1, 10001, 2):
            yt[k,:] += -4 / (n * np.pi) * np.exp(-(n * np.pi)**2 * time[k]) * np.sin(n * np.pi * x)

    x_dim = x * L
    T = (yt+1)
    time_dim = time * L**2 / (alpha)

    for i in range(time_dim.shape[0]):
        plt.plot(x_dim, T[i], label="{:.6f}".format(time_dim[i]))

    # naming the x axis
    plt.xlabel('x')
    # naming the y axis
    plt.ylabel('T')
    plt.legend()

    # function to show the plot
    plt.show()
    plt.close()

    return x_dim, T




x_dim, T = plot_analytical(1, 0.01, 1e-1, 10, 1)

print(np.min(T[-1]))