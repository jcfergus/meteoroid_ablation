from pyatmos import expo
import numpy as np
import constants as const
from scipy.interpolate import CubicSpline, interp1d
import matplotlib.pyplot as plt
from timer import Timer
import pickle as pkl
import time
from pyatmos import download_sw_nrlmsise00,read_sw_nrlmsise00
from pyatmos import nrlmsise00




def dump_NRLMSISE_data():
    time = Timer()

    swfile = download_sw_nrlmsise00()
    swdata = read_sw_nrlmsise00(swfile)

    time.reset()

    # Set a specific time and location
    years = ['1991', '1993', '1995', '1997', '1999', '2001']
    months = ['01', '04', '07', '10']

    height_km = np.arange(0, 501, 1)
    num_density = np.zeros((7, height_km.shape[0]))
    Temperature = np.zeros((height_km.shape[0]))
    Pressure = np.zeros((height_km.shape[0]))
    Density = np.zeros((height_km.shape[0]))

    index = 0
    for height in height_km:
        for year in years:
            for month in months:
                t = year + '-' + month + '-1 03:00:00'  # time(UTC)
                lat, lon, alt = 45, 75, height  # latitude, longitude in [degree], and altitude in [km]
                nrl00 = nrlmsise00(t, (lat, lon, alt), swdata)
                num_density[0, index] += nrl00.nd['O2']
                num_density[1, index] += nrl00.nd['N2']
                num_density[2, index] += nrl00.nd['He']
                num_density[3, index] += nrl00.nd['Ar']
                num_density[4, index] += nrl00.nd['N']
                num_density[5, index] += nrl00.nd['O']
                num_density[6, index] += nrl00.nd['H']
                Temperature[index] += nrl00.T
                Density[index] += nrl00.rho

                # print(nrl00.rho)  # [kg/m^3]
                # print(nrl00.T)  # [K]
                # print(nrl00.nd)  # composition in [1/m^3]
                # print(nrl00.T)
        index += 1
        print(index)

    num_density.tofile('numdensity_500.dat')

    # num_density = np.fromfile('numdensity_500.dat')
    num_density /= 24.
    Temperature /= 24.
    Density /= 24.


    print("Temperature = [", end="")
    for i in range(501):
        print(Temperature[i],end=",")
    print("]:")

    print("Density = [", end="")
    for i in range(501):
        print(Density[i],end=",")
    print("]:")

    # Pressure =

    filename = 'species_numdensity_500_NRLMSISE-00'
    fileObject = open(filename, 'wb')
    pkl.dump(num_density, fileObject)
    fileObject.close()

def test_NRLMSISE():

    time = Timer()

    height_km = np.arange(0, 501, 1)

    filename = 'species_numdensity_500_NRLMSISE-00'
    fileObject = open(filename, 'rb')
    num_density = pkl.load(fileObject)
    fileObject.close()

    linear_o2 = interp1d(height_km, num_density[0, :])
    linear_n2 = interp1d(height_km, num_density[1, :])
    linear_he = interp1d(height_km, num_density[2, :])
    linear_ar = interp1d(height_km, num_density[3, :])
    linear_n = interp1d(height_km, num_density[4, :])
    linear_o = interp1d(height_km, num_density[5, :])
    linear_h = interp1d(height_km, num_density[6, :])

    height_km_2 = np.arange(0, 500, 0.5)
    num_density_2 = np.zeros((7, height_km_2.shape[0]))

    time.reset()

    index = 0
    for height in height_km_2:
        num_density_2[0, index] = linear_o2(height)
        num_density_2[1, index] = linear_n2(height)
        num_density_2[2, index] = linear_he(height)
        num_density_2[3, index] = linear_ar(height)
        num_density_2[4, index] = linear_n(height)
        num_density_2[5, index] = linear_o(height)
        num_density_2[6, index] = linear_h(height)
        index += 1

    print("cubic spline time: ", time.elapsed())

    print(num_density[5, :])
    print(num_density_2[5, :])

    fig, ax = plt.subplots()

    ax.plot(height_km_2, num_density_2[0,:], label="O2 - spline")
    ax.plot(height_km_2, num_density_2[1,:], label="N2 - spline")
    ax.plot(height_km_2, num_density_2[2,:], label="He - spline")
    ax.plot(height_km_2, num_density_2[3,:], label="Ar - spline")
    ax.plot(height_km_2, num_density_2[4,:], label="N - spline")
    ax.plot(height_km_2, num_density_2[5,:], label="O - spline")
    ax.plot(height_km_2, num_density_2[6,:], label="H - spline")

    ax.set(xlabel="height (km)", ylabel="number density")
    # ax.grid()
    ax.set_yscale('log')
    plt.legend()
    plt.show()

    rho_O2 = num_density[0, :] * 32e-3 / const.Na
    rho_N2 = num_density[1, :] * 28e-3 / const.Na
    rho_He = num_density[2, :] * 4e-3 / const.Na
    rho_Ar = num_density[3, :] * 40e-3 / const.Na
    rho_N = num_density[4, :] * 14e-3 / const.Na
    rho_O1 = num_density[5, :] * 16e-3 / const.Na
    rho_H = num_density[6, :] * 1e-3 / const.Na

    rho_total = rho_O2 + rho_N2 + rho_H + rho_N + rho_Ar + rho_He + rho_O1
    rho_spline = 10 ** (cs(height_km))
    rho_pyatmos = expo(height_km).rho

    mask = (height_km > 100) & (height_km < 150)
    print(np.mean(rho_total[mask] / rho_pyatmos[mask]))

    fig, ax = plt.subplots()
    ax.plot(height_km, rho_total, label="Summed Species")
    ax.plot(height_km, rho_spline, label="Cubic Spline")
    ax.plot(height_km, rho_pyatmos, label="pyatmos exponential")

    ax.set(xlabel="height (km)", ylabel="Density")
    ax.grid()
    ax.set_yscale('log')
    plt.legend()
    plt.show()



def plot_NRLMSISE():
    swfile = download_sw_nrlmsise00()
    swdata = read_sw_nrlmsise00(swfile)


    # Set a specific time and location
    years = ['1991', '1993', '1995', '1997', '1999', '2001']
    months = ['01', '04', '07', '10']

    height_km = np.arange(0,500,5)
    num_density = np.zeros((7,height_km.shape[0]))

    index = 0
    for height in height_km:
        for year in years:
            for month in months:
                t = year+'-'+month+'-1 03:00:00'  # time(UTC)
                lat, lon, alt = 45, 75, height  # latitude, longitude in [degree], and altitude in [km]
                nrl00 = nrlmsise00(t, (lat, lon, alt), swdata)
                num_density[0,index] += nrl00.nd['O2']
                num_density[1,index] += nrl00.nd['N2']
                num_density[2,index] += nrl00.nd['He']
                num_density[3,index] += nrl00.nd['Ar']
                num_density[4,index] += nrl00.nd['N']
                num_density[5,index] += nrl00.nd['O']
                num_density[6,index] += nrl00.nd['H']
                # print(nrl00.rho)  # [kg/m^3]
                # print(nrl00.T)  # [K]
                # print(nrl00.nd)  # composition in [1/m^3]
        print(index)
        index += 1

    num_density /= 24

    fig, ax = plt.subplots()
    ax.plot(height_km, num_density[0,:], label="O2")
    ax.plot(height_km, num_density[1,:], label="N2")
    ax.plot(height_km, num_density[2,:], label="He")
    ax.plot(height_km, num_density[3,:], label="Ar")
    ax.plot(height_km, num_density[4,:], label="N")
    ax.plot(height_km, num_density[5,:], label="O")
    ax.plot(height_km, num_density[6,:], label="H")

    ax.set(xlabel="height (km)", ylabel="number density")
    # ax.grid()
    ax.set_yscale('log')
    plt.legend()
    plt.show()

    rho_O2 = num_density[0,:] * 32e-3 / const.Na
    rho_N2 = num_density[1,:] * 28e-3 / const.Na
    rho_He = num_density[2,:] * 4e-3 / const.Na
    rho_Ar = num_density[3,:] * 40e-3 / const.Na
    rho_N = num_density[4,:] * 14e-3 / const.Na
    rho_O1 = num_density[5,:] * 16e-3 / const.Na
    rho_H = num_density[6,:] * 1e-3 / const.Na

    rho_total = rho_O2 + rho_N2 + rho_H + rho_N + rho_Ar + rho_He + rho_O1
    rho_spline = 10 ** (cs(height_km))
    rho_pyatmos = expo(height_km).rho

    mask = (height > 100000) & (height < 150000)

    print(np.mean(rho_total[mask] / rho_pyatmos[mask]))

    fig, ax = plt.subplots()
    ax.plot(height_km, rho_total, label="Summed Species")
    ax.plot(height_km, rho_spline, label="Cubic Spline")
    ax.plot(height_km, rho_pyatmos, label="pyatmos exponential")

    ax.set(xlabel="height (km)", ylabel="Density")
    ax.grid()
    ax.set_yscale('log')
    plt.legend()
    plt.show()



x_spline = np.array(
    [2.70747795651882, 8.79551726458908, 13.8688833546477, 18.6040250387023, 23.0009423167531, 27.3978595948038,
     32.4712256848624, 37.8828161809248, 43.2944066769873, 48.3677727670458, 54.1175876691122, 60.8820757891903,
     66.6318906912566, 71.7052567813152, 76.4403984653699, 80.4990913374167, 84.5577842094636, 88.6164770815104,
     92.6751699535573, 96.7338628256041, 101.130780103655, 105.86592178771, 110.26283906576, 114.997980749815,
     120.409571245877, 126.835834959952, 134.276771892037, 141.717708824123, 149.158645756209, 156.599582688295,
     164.040519620381, 171.481456552467, 178.922393484553, 186.363330416639, 193.804267348725, 201.24520428081,
     208.686141212896, 216.127078144982, 223.568015077068, 231.008952009154, 238.44988894124, 245.890825873326,
     253.331762805412, 260.772699737498, 268.213636669583, 275.654573601669, 283.095510533755, 290.536447465841,
     297.977384397927, 305.418321330013, 312.859258262099, 320.300195194185, 327.74113212627, 335.182069058356,
     342.623005990442, 350.063942922528, 357.504879854614, 364.9458167867, 372.386753718786, 379.827690650872,
     387.268627582958, 394.709564515043, 402.150501447129, 409.591438379215, 417.032375311301, 424.473312243387,
     431.914249175473, 439.355186107559, 446.796123039645, 454.237059971731, 461.677996903816, 469.118933835902,
     476.559870767988, 484.000807700074, 491.44174463216, 497.868008346234])
y_spline = np.array(
    [-0.050914895160705, -0.365911630269172, -0.663908541578134, -0.983905224862927, -1.29223536240296,
     -1.59989884018615, -1.95489516070522, -2.30664151490967, -2.63038815932671, -2.91988515873592,
     -3.22188202858594, -3.55227860407749, -3.85587545734393, -4.2001218892839, -4.5495349343394,
     -4.85986505114997, -5.17719509540739, -5.50385837626061, -5.84218820285859, -6.15951824711601,
     -6.50384801152559, -6.83384459116303, -7.16050787201625, -7.43583835159254, -7.7490851048393,
     -8.06074096548449, -8.35346520417115, -8.58000831063299, -8.76455185241371, -8.91091397175704,
     -9.05091252069414, -9.18963835554999, -9.29018276796845, -9.39963617895563, -9.50781687586157,
     -9.60327043195504, -9.69363313172353, -9.79290483006075, -9.88454024391048, -9.9812665140852,
     -10.0741746420162, -10.1569010572972, -10.2345366162532, -10.3185357456155, -10.3847168778403,
     -10.4648978649588, -10.5387152816711, -10.6036236998147, -10.6698048320395, -10.7410768205893,
     -10.8072579528141, -10.8683482287139, -10.9383475031824, -11.0058013494885, -11.0656189113071,
     -11.1330727576131, -11.1865267490255, -11.2463443108441, -11.3010710163376, -11.3621612922375,
     -11.4168879977311, -11.4652511328184, -11.5161596960683, -11.5708864015619, -11.6192495366492,
     -11.6739762421428, -11.7248848053927, -11.7757933686425, -11.8292473600549, -11.8839740655485,
     -11.9387007710421, -11.9985183328606, -12.0430633257043, -12.1003354593603, -12.1665165915852,
     -12.213288834071])

cs = CubicSpline(x_spline, y_spline)

O2_73 = np.polynomial.polynomial.Polynomial(np.array([6.10057e1, -7.12746e-5, -4.53546e-9, 9.27519e-14, -5.92157e-19]))
O2_160 = np.polynomial.polynomial.Polynomial(np.array([-3.14482e1, 3.37054e-3, -4.77364e-8, 2.74203e-13, -5.64601e-19]))
O2_500 = np.polynomial.polynomial.Polynomial(np.array([4.90062e1, -7.85865e-5, 1.10253e-10, -8.01809e-17, 0, 0]))

N2_73 = np.polynomial.polynomial.Polynomial(
    np.array([6.23243e1, -7.22646e-5, -4.46171e-9, 9.09153e-14, -5.77755e-19, 0]))
N2_160 = np.polynomial.polynomial.Polynomial(
    np.array([-1.47517e1, 2.85208e-3, -4.15926e-8, 2.44109e-13, -5.11749e-19, 0]))
N2_500 = np.polynomial.polynomial.Polynomial(np.array([5.01497e1, -6.84997e-5, 9.49379e-11, -6.87984e-17, 0, 0]))

He_73 = np.polynomial.polynomial.Polynomial(
    np.array([5.04079e1, -7.04124e-5, -4.62672e-9, 9.63929e-14, -6.53449e-19, 3.68169e-25]))
He_160 = np.polynomial.polynomial.Polynomial(
    np.array([-2.13316e2, 1.19401e-2, -2.13827e-7, 1.82496e-12, -7.51651e-18, 1.20426e-23]))
He_500 = np.polynomial.polynomial.Polynomial(np.array([3.64817e1, -1.22088e-5, 1.78344e-11, -1.46951e-17, 0, 0]))

Ar_73 = np.polynomial.polynomial.Polynomial(
    np.array([5.78961e1, -7.14397e-5, -4.52834e-9, 9.27131e-14, -5.93026e-19, 0]))
Ar_160 = np.polynomial.polynomial.Polynomial(
    np.array([-3.63307e1, 3.42062e-3, -4.81298e-8, 2.74488e-13, -5.62124e-19, 0]))
Ar_500 = np.polynomial.polynomial.Polynomial(np.array([4.74112e1, -1.01128e-4, 1.55725e-10, -1.19261e-16, 0, 0]))

N_73 = np.polynomial.polynomial.Polynomial(np.array([0, 0, 0, 0, 0, 0]))
N_160 = np.polynomial.polynomial.Polynomial(
    np.array([-1.64974e2, 8.44859e-3, -1.44755e-7, 1.21689e-12, -4.98611e-18, 7.98039e-24]))
N_500 = np.polynomial.polynomial.Polynomial(
    np.array([-1.16933, 5.71683e-4, -3.41097e-9, 9.69120e-15, -1.34938e-20, 7.391632e-27]))

O_73 = np.polynomial.polynomial.Polynomial(np.array([0, 0, 0, 0, 0, 0]))
O_115 = np.polynomial.polynomial.Polynomial(
    np.array([-1.03496e2, 1.06507e-3, 5.11605e-8, -7.45582e-13, 2.75453e-18, 0]))
O_160 = np.polynomial.polynomial.Polynomial(np.array([7.22170e1, -5.033149e-4, 2.84499e-9, -5.72513e-15, 0, 0]))
O_500 = np.polynomial.polynomial.Polynomial(np.array([4.61301e1, -3.90036e-5, 5.03715e-11, -3.69961e-17, 0, 0]))

H_73 = np.polynomial.polynomial.Polynomial(np.array([0, 0, 0, 0, 0, 0]))
H_83 = np.polynomial.polynomial.Polynomial(np.array([1.27188e3, -5.69682e-2, 8.34563e-7, -3.94517e-12, 0, 0]))
H_160 = np.polynomial.polynomial.Polynomial(
    np.array([-7.63952e1, 5.67971e-3, -1.05546e-7, 9.29242e-13, -3.97120e-18, 6.65029e-24]))
H_500 = np.polynomial.polynomial.Polynomial(
    np.array([4.87925e1, -2.64976e-4, 1.51842e-9, -4.30964e-15, 6.02362e-21, -3.31696e-27]))


def get_density_spline(height):
    return 10 ** cs(height * 0.001)


def get_density_atmos(height):
    return expo(height * 0.001).rho


def get_O2_rogers(height):
    if height < 73000:
        return np.exp(O2_73(height))
    if height < 160000:
        return np.exp(O2_160(height))
    if height < 500000:
        return np.exp(O2_500(height))


def get_N2_rogers(height):
    if height < 73000:
        return np.exp(N2_73(height))
    if height < 160000:
        return np.exp(N2_160(height))
    if height < 500000:
        return np.exp(N2_500(height))


def get_He_rogers(height):
    if height < 73000:
        return np.exp(He_73(height))
    if height < 160000:
        return np.exp(He_160(height))
    if height < 500000:
        return np.exp(He_500(height))


def get_Ar_rogers(height):
    if height < 73000:
        return np.exp(Ar_73(height))
    if height < 160000:
        return np.exp(Ar_160(height))
    if height < 500000:
        return np.exp(Ar_500(height))


def get_N_rogers(height):
    if height < 73000:
        return np.exp(N_73(height))
    if height < 160000:
        return np.exp(N_160(height))
    if height < 500000:
        return np.exp(N_500(height))


def get_O_rogers(height):
    if height < 73000:
        return np.exp(O_73(height))
    if height < 115000:
        return np.exp(O_115(height))
    if height < 160000:
        return np.exp(O_160(height))
    if height < 500000:
        return np.exp(O_500(height))


def get_H_rogers(height):
    if height < 73000:
        return np.exp(H_73(height))
    if height < 83000:
        return np.exp(H_83(height))
    if height < 160000:
        return np.exp(H_160(height))
    if height < 500000:
        return np.exp(H_500(height))


def plot_num_density():
    height = np.arange(0, 500000, 1000)
    height_km = height * 0.001
    O2 = np.zeros(height.shape)
    N2 = np.zeros(height.shape)
    He = np.zeros(height.shape)
    Ar = np.zeros(height.shape)
    N = np.zeros(height.shape)
    O1 = np.zeros(height.shape)
    H = np.zeros(height.shape)

    for i in range(height.shape[0]):
        O2[i] = get_O2_rogers(height[i])
        N2[i] = get_N2_rogers(height[i])
        He[i] = get_He_rogers(height[i])
        Ar[i] = get_Ar_rogers(height[i])
        N[i] = get_N_rogers(height[i])
        O1[i] = get_O_rogers(height[i])
        H[i] = get_H_rogers(height[i])

    fig, ax = plt.subplots()
    ax.plot(height_km, O2, label="O2")
    ax.plot(height_km, N2, label="N2")
    ax.plot(height_km, He, label="He")
    ax.plot(height_km, Ar, label="Ar")
    ax.plot(height_km, N, label="N")
    ax.plot(height_km, O1, label="O")
    ax.plot(height_km, H, label="H")

    ax.set(xlabel="height (km)", ylabel="number density")
    ax.grid()
    ax.set_yscale('log')
    plt.legend()
    plt.show()


def plot_density_comparison():
    height = np.arange(0, 500000, 1000)
    height_km = height * 0.001
    O2 = np.zeros(height.shape)
    N2 = np.zeros(height.shape)
    He = np.zeros(height.shape)
    Ar = np.zeros(height.shape)
    N = np.zeros(height.shape)
    O1 = np.zeros(height.shape)
    H = np.zeros(height.shape)

    for i in range(height.shape[0]):
        O2[i] = get_O2_rogers(height[i])
        N2[i] = get_N2_rogers(height[i])
        He[i] = get_He_rogers(height[i])
        Ar[i] = get_Ar_rogers(height[i])
        N[i] = get_N_rogers(height[i])
        O1[i] = get_O_rogers(height[i])
        H[i] = get_H_rogers(height[i])

    rho_O2 = O2 * 32e-3 / const.Na
    rho_N2 = N2 * 28e-3 / const.Na
    rho_He = He * 4e-3 / const.Na
    rho_Ar = Ar * 40e-3 / const.Na
    rho_N = N * 14e-3 / const.Na
    rho_O1 = O1 * 16e-3 / const.Na
    rho_H = H * 1e-3 / const.Na

    rho_total = rho_O2 + rho_N2 + rho_H + rho_N + rho_Ar + rho_He + rho_O1
    rho_spline = 10 ** (cs(height_km))
    rho_pyatmos = expo(height_km).rho

    mask = (height > 100000) & (height < 150000)

    print(np.mean( rho_total[mask] / rho_pyatmos[mask] ))

    fig, ax = plt.subplots()
    ax.plot(height_km, rho_total, label="Summed Species")
    ax.plot(height_km, rho_spline, label="Cubic Spline")
    ax.plot(height_km, rho_pyatmos, label="pyatmos exponential")

    ax.set(xlabel="height (km)", ylabel="Density")
    ax.grid()
    ax.set_yscale('log')
    plt.legend()
    plt.show()



dump_NRLMSISE_data()