epsilon = 0.9 # Emissivity
Gamma = 1 # Drag coefficient
Lambda = 1 # Heat Transfer coefficient
sigma = 5.67e-8 # Stefan Boltzmann constant
A = 1.21 # Shape factor
C_A = 10.6 # Clausius-Clapeyron coefficient
C_B = 13500 # Clausius-Clapeyron coefficient
k = 1.381e-23 # boltzmann constant
Na = 6.022e23
a0 = 5.29177210903e-11
eV = 1.60217662e-19
e_cgs = 4.80320425e-10

def eV_to_cgs(eV):
    return eV / 6.2415e11

def eV_to_SI(eV):
    return eV * 1.602176e-19

