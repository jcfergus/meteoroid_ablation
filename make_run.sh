cd "${0%/*}" || exit 1    # Run from this directory
set -e  # exit when any command fails

eval "$(conda shell.bash hook)"  # this is required for conda
conda activate puma

mkdir -p build
mkdir -p out
mkdir -p bin
cd build
mkdir -p out
cmake -D FILENAME=$1 -D CONDA_PREFIX=$CONDA_PREFIX -D CMAKE_INSTALL_PREFIX=../bin ..
make
make install


../bin/meteoroid_ablation
